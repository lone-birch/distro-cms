export const fakeContent = {
  _path: '/',
  _dir: '',
  _draft: false,
  _partial: false,
  _locale: '',
  cms: {
    pageTitle: 'Sergio Ruiz - Software Engineer',
    pageDescription: 'Software Engineer based in Madrid',
    plainTextElements: {
      hero: {
        label: 'Hero title',
        text: 'Welcome to our storessss'
      }
    },
    markdownElements: {
      description: {
        label: 'Hero Description',
        markdown: "I'm a **Software Engineer** based in _Madrid_.",
      },
      cta: {
        label: 'Call To Action',
        markdown: 'Get in touch',
      },
    },
  },
  _id: 'content:index.json',
  _type: 'json',
  label: '',
  _source: 'content',
  _file: 'index.json',
  _extension: 'json',
};

export const anotherFakeContent = {
  _path: '/about',
  _dir: '',
  _draft: false,
  _partial: false,
  _locale: '',
  cms: {
    pageTitle: 'About — Sergio Ruiz',
    pageDescription: 'Software Engineer based in Madrid',
    plainTextElements: {
      hero: {
        label: 'Hero titlesssss',
        text: 'Welcome to our storesssss'
      }
    },
    markdownElements: {
      about: {
        label: 'About Me',
        markdown: "I'm just a human being.",
      },
    },
  },
  _id: 'content:about.json',
  _type: 'json',
  label: 'About',
  _source: 'content',
  _file: 'about.json',
  _extension: 'json',
};
