import type { Meta, StoryObj } from '@storybook/vue3';

import { PageWrapper, Nav } from '~/components';
import { fakeContent } from '~/stories/fake-data';

type Story = StoryObj<typeof Nav>;

const meta: Meta<typeof Nav> = {
  title: 'Editor/Navigation',
  component: Nav,
};
export default meta;

export const Navigation: Story = {
  render: (args) => ({
    components: { Nav, PageWrapper },
    setup() {
      return { fakeContent, args };
    },
    template: `
      <PageWrapper :content='fakeContent'>
        <Nav v-bind='args' />
      </PageWrapper>

      <Component is='style'>
        body {
        background-color: #202020;
        }
      </Component>
    `,
  }),
};
