import type { Meta, StoryObj } from '@storybook/vue3';

import { IconButton as IconButtonComponent } from '~/components';
import { PhFloppyDisk } from '@phosphor-icons/vue';

type Story = StoryObj<typeof IconButtonComponent>;

const meta: Meta<typeof IconButtonComponent> = {
  title: 'IconButton',
  component: IconButtonComponent,
  argTypes: {
    theme: {
      options: ['dark', 'light'],
    },
    unsavedCount: {
      type: 'number',
    },
    type: {
      options: ['button', 'submit', 'reset'],
    },
  },
};
export default meta;

export const IconButton: Story = {
  render: (args, { argTypes }) => ({
    components: {
      IconButton: IconButtonComponent,
      PhFloppyDisk,
    },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
        <div>
          <IconButton v-bind='args'>
            <PhFloppyDisk />
          </IconButton>

          <Component is='style' v-if="args.theme === 'dark'">
            body {
              background-color: #202020;
            }
          </Component>
        </div>
      `,
  }),
  args: {
    theme: 'light',
    editing: false,
    unsavedCount: 0,
    type: 'button',
  },
};
