import type { Meta, StoryObj } from '@storybook/vue3';

import { computed, defineComponent, ref } from 'vue';
import { PageEditor as PageEditorComponent, PageWrapper, LoginForm } from '~/components';
import type { LoginResponse, UnsavedChangesType } from '~/types';
import { useGlobalCMSState } from '~/composables';
import { md } from '~/utils';
import { fakeContent, anotherFakeContent } from '~/stories/fake-data';

type Story = StoryObj<typeof PageEditorComponent>;

const meta: Meta<typeof PageEditorComponent> = {
  title: 'Editor/PageEditor',
  component: PageEditorComponent,
};
export default meta;

const NuxtMockPage = defineComponent({
  setup() {
    const { pageContent } = useGlobalCMSState();

    const mdElements = computed(() => pageContent.value?.cms?.markdownElements ?? {});

    return { pageContent, mdElements, md };
  },
  template: `
    <div>
      <p
        data-cms-md
        data-cms="description"
        v-html="md(mdElements.description)"
      ></p>
    </div>
  `,
});

export const PageEditor: Story = {
  render: () => ({
    components: { PageWrapper, LoginForm, NuxtMockPage },
    setup() {
      const content = ref<any>(fakeContent);

      function changeContent() {
        content.value = anotherFakeContent;
        console.log(content.value);
      }

      function setContentToNull() {
        content.value = null;
      }

      function login() {
        return new Promise<LoginResponse>((resolve) =>
          setTimeout(
            () =>
              resolve({
                token: 'fake-token',
                refreshToken: 'fake-refresh-token',
                refreshesAt: Date.now() + 60 * 60 * 1000,
                expiresAt: Date.now() + 7 * 24 * 60 * 60 * 1000,
              }),
            1000,
          ),
        );
      }

      function save(changes: UnsavedChangesType) {
        console.log('Saving changes:', JSON.parse(JSON.stringify(changes)));
        return new Promise<boolean>((resolve) =>
          setTimeout(() => {
            resolve(true);
          }, 1000),
        );
      }

      return { content, login, changeContent, save, setContentToNull };
    },
    template: `
      <PageWrapper :content="content" :save="save" route="/" show-editor>
        <template #login-form>
          <LoginForm :login="login" />
        </template>

        <NuxtMockPage />

        <hr />

        <button
          class="dui"
          @click="changeContent"
          style="background-color: #445bdd; color: white; padding: 10px 16px; border-radius: 8px"
        >
          Change content
        </button>

        <button
          class="dui"
          @click="setContentToNull"
          style="background-color: #e7610f; color: white; padding: 10px 16px; border-radius: 8px;"
        >
          Set content to null
        </button>
      </PageWrapper>
    `,
  }),
};
