import { marked } from 'marked';
import type { MarkdownElement, PlainTextElement } from '~/types';

marked.setOptions({
  breaks: true
});

export function md(data: string | MarkdownElement | undefined | null) {
  if (!data) return '';

  const renderer = new marked.Renderer();
  renderer.paragraph = (text: string) => {
    // Wrap the text in paragraph tags
    return `<p>${text}</p>`;
  };
  marked.use({ renderer });
  return marked.parse(typeof data === 'string' ? data : data.markdown);
}

export function text(data: string | PlainTextElement | undefined | null) {
  if (!data) return '';
  return typeof data === 'string' ? data : data.text;
}