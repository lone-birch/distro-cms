import { Ref } from 'vue';
import { isEqual } from 'lodash';
import { localStorageWrapper } from '~/utils';
import type { LocalStorageCMSValue, PageContentType, UnsavedChangesType } from '~/types';

export function evaluateUnsavedChanges(
  contentProp: PageContentType | null,
  unsavedChanges: Ref<UnsavedChangesType>,
) {
  if (contentProp) {
    const locallySavedContent = localStorageWrapper.getItem<LocalStorageCMSValue>('DistroCMS');

    if (locallySavedContent) {
      const pages = Object.keys(locallySavedContent);
      for (const page of pages) {
        const originalContent = locallySavedContent[page]!.original;
        const contentChanges = locallySavedContent[page]!.changes;
        const __filePath = locallySavedContent[page]!.__filePath;

        if (originalContent && contentChanges) {
          if (!isEqual(originalContent, contentChanges)) {
            unsavedChanges.value[page] = { ...contentChanges, __filePath, __isSelected: true };
          } else {
            delete unsavedChanges.value[page];
          }
        }
      }
      return;
    }

    localStorageWrapper.setItem<LocalStorageCMSValue>('DistroCMS', {});
  }
}
