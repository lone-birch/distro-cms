import type { Ref } from 'vue';
import { isEqual } from 'lodash';
import { localStorageWrapper } from '~/utils';
import type { LocalStorageCMSValue, PageContentType } from '~/types';

export function checkLocalStorage(
  contentProp: PageContentType | null,
  pageContent: Ref<PageContentType | null>,
) {
  if (contentProp) {
    let locallySavedContent = localStorageWrapper.getItem<LocalStorageCMSValue>('DistroCMS');
    if (!locallySavedContent) {
      localStorageWrapper.setItem<LocalStorageCMSValue>('DistroCMS', {});
      locallySavedContent = {};
    }

    const originalContent = locallySavedContent?.[contentProp._path!]?.original;
    const contentChanges = locallySavedContent?.[contentProp._path!]?.changes;

    if (originalContent) {
      if (!isEqual(originalContent, contentProp.cms)) {
        localStorageWrapper.setItem<LocalStorageCMSValue>('DistroCMS', {
          ...(locallySavedContent ?? {}),
          [contentProp._path!]: {
            ...locallySavedContent[contentProp._path!]!,
            original: contentProp.cms,
          },
        });
      }
    }

    if (contentChanges) {
      pageContent.value = {
        ...contentProp,
        cms: contentChanges,
      };
    } else {
      pageContent.value = JSON.parse(JSON.stringify(contentProp));
    }

    return;
  }

  pageContent.value = null;
}
