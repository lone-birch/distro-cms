const localStorageWrapper = {
  setItem<T extends object>(key: string, value: T) {
    window.localStorage.setItem(key, JSON.stringify(value));
    document.dispatchEvent(new Event('local-storage'));
  },
  getItem<T>(key: string): T | null {
    const json = window.localStorage.getItem(key);
    return json ? (JSON.parse(json) as T) : null;
  },
  clear() {
    window.localStorage.clear();
    document.dispatchEvent(new Event('local-storage'));
  },
  removeItem(key: string) {
    window.localStorage.removeItem(key);
    document.dispatchEvent(new Event('local-storage'));
  },
};

export default localStorageWrapper;
