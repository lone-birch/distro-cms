import type { DeepReadonly, Ref } from 'vue';

export type Nullable<T> = {
  [K in keyof T]: T[K] | null;
};

export type EditorMode = 'editing' | 'changes';

export type PageContentType = ParsedContent & PageContent;

export interface CMSStateProvider {
  showEditor: Ref<boolean>;
  editorMode: Ref<EditorMode>;
  isLoggedIn: Ref<boolean>;
  pageContent: DeepReadonly<Ref<PageContentType | null>>;
  unsavedChanges: Ref<UnsavedChangesType>;

  updatePageContent: (newContent: PageContent['cms']) => void;
  saveChanges: (
    changes: UnsavedChangesType,
    cookies: Nullable<LoginResponse>,
  ) => Promise<string | undefined>;
  undoChanges: (specificPage?: string) => void;
  resetPageContent: () => void;
  logout: () => void;
}

export interface PageContent {
  cms: {
    pageTitle: string;
    pageDescription: string;
    plainTextElements: {
      [key: string]: PlainTextElement;
    };

    markdownElements: {
      [key: string]: MarkdownElement;
    };
  };
}

export interface PlainTextElement {
  label: string;
  text: string;
}

export interface MarkdownElement {
  label: string;
  markdown: string;
}

export type UnsavedChangesType = {
  [pageRoute: string]: PageContent['cms'] & { __isSelected: boolean; __filePath: string };
};

export type LocalStorageCMSValue = {
  [pageRoute: string]:
    | {
        original: PageContent['cms'];
        changes: PageContent['cms'] | null;
        __filePath: string;
      }
    | undefined;
};

export type MarkdownElements<T> = Record<keyof T, MarkdownElement>;
export type PlainTextElements<T> = Record<keyof T, PlainTextElement>;

export type LoginResponse = {
  token: string;
  refreshToken: string;
  refreshesAt: number;
  expiresAt: number;
};

// Copied from the `@nuxt/content` package
export interface ParsedContent {
  /**
   * Content id
   */
  _id: string;
  /**
   * Content source
   */
  _source?: string;
  /**
   * Content path, this path is source agnostic and it the content my live in any source
   */
  _path?: string;
  /**
   * Content title
   */
  title?: string;
  /**
   * Content draft status
   */
  _draft?: boolean;
  /**
   * Content partial status
   */
  _partial?: boolean;
  /**
   * Content locale
   */
  _locale?: string;
  /**
   * File type of the content, i.e `markdown`
   */
  _type?: 'markdown' | 'yaml' | 'json' | 'csv';
  /**
   * Path to the file relative to the content directory
   */
  _file?: string;
  /**
   * Extension of the file
   */
  _extension?: 'md' | 'yaml' | 'yml' | 'json' | 'json5' | 'csv';
}
