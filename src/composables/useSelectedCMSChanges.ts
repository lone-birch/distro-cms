import { computed } from 'vue';
import { useGlobalCMSState } from '~/composables';

export default function useSelectedCMSChanges() {
  const { unsavedChanges } = useGlobalCMSState();
  return computed(() => {
    return Object.values(unsavedChanges.value).filter((change) => change.__isSelected);
  });
}
