import { inject } from 'vue';
import type { CMSStateProvider } from '~/types';

function useGlobalCMSState() {
  return inject<CMSStateProvider>('DistroCMS')!;
}

export default useGlobalCMSState;
