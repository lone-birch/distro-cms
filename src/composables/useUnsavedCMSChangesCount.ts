import { computed } from 'vue';
import { useGlobalCMSState } from '~/composables';

export default function useUnsavedCMSChangesCount() {
  const { unsavedChanges } = useGlobalCMSState();
  return computed(() => {
    return Object.keys(unsavedChanges.value).length;
  });
}
