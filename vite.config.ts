import path from 'node:path';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import dts from 'vite-plugin-dts';
import eslint from 'vite-plugin-eslint';
import { viteStaticCopy as copy } from 'vite-plugin-static-copy';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    dts({
      exclude: ['src/types.ts', 'src/stories'],
    }),
    copy({
      targets: [{ src: 'src/types.ts', dest: '.' }],
    }),
    eslint(),
  ],
  resolve: {
    alias: {
      '~': path.resolve(__dirname, 'src'),
    },
  },
  build: {
    cssMinify: false,
    lib: {
      entry: path.resolve(__dirname, 'src/index.ts'),
      formats: ['es'],
      fileName: 'index',
    },
    rollupOptions: {
      external: ['vue', '@distromade/distro-ui'],
    },
  },
});
